const getCustomers = require("./getCustomers");
const calculateDistance = require("./calculateDistance");
const OFFICE_COORDINATES = { latitude: "53.3393", longitude: "-6.2576841" };

function filterCustomersList(customers) {
  const sortedCustomersWithDistancesWithinRange = customers
    .map(customer =>
      Object.assign({}, customer, {
        distance_to_office: calculateDistance(customer, OFFICE_COORDINATES)
      })
    )
    .filter(customer => customer.distance_to_office <= 100)
    .sort((customerA, customerB) => customerA.user_id - customerB.user_id);

  return sortedCustomersWithDistancesWithinRange;
}

function printInvitations(customersList) {
  customersList.forEach(({ name, user_id, distance_to_office }) => {
    console.log(name, user_id);
  });
}

getCustomers().then(filterCustomersList).then(printInvitations).catch(error => {
  console.log("Sorry, we were unable to generate invitations.", error);
});
