const { processRawCustomersList } = require("./getCustomers");

const cases = [
  {
    input: '{ "test" : "1"} \n { "test": "2"}',
    expected: [{ test: "1" }, { test: "2" }]
  },
  {
    input: " \n ",
    expected: []
  }
];

try {
  cases.forEach(({ input, expected }) => {
    const result = processRawCustomersList(input);
    if (Object.keys(result).length !== expected.length) {
      throw "processed result contains wrong number of items";
    }
  });
  console.log("Success");
} catch (e) {
  console.log("Error", e);
}
