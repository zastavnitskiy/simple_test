function flatten(inputArray) {
  let result = [];

  for (let i = 0; i < inputArray.length; i++) {
    if (Array.isArray(inputArray[i])) {
      result = result.concat(flatten(inputArray[i]));
    } else {
      result.push(inputArray[i]);
    }
  }

  return result;
}

module.exports = flatten;
