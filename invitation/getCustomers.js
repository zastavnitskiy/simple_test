const fs = require("fs");
const path = require("path");
const CUSTOMERS_LIST_PATH = path.join(__dirname, "./customers");

//the task stated json formatted data, however, it's not
//the proper solution will be to fix the data,
//but for the sake of excercise, let's parse the format we have
function processRawCustomersList(rawInput) {
  const customersList = rawInput
    .split("\n")
    .map(rawString => rawString.trim())
    .filter(rawString => Boolean(rawString))
    .map(rawString => JSON.parse(rawString));
  return customersList;
}

//this function returns us a list of customers. I used promise here to
//avoid making an extra assumption on where data source is syncronous or not.
//this way we can easily change underlying implementation to db call or fetching
//a third party API without changing other parts of the app
function getCustomers() {
  return new Promise((resolve, reject) => {
    fs.readFile(CUSTOMERS_LIST_PATH, "utf8", (error, rawInput) => {
      if (error) {
        reject("Error when reading the data from disk");
      }

      try {
        const customersList = processRawCustomersList(rawInput);
        resolve(customersList);
      } catch (e) {
        reject("Error when parsing raw data");
      }
    });
  });
}

module.exports = getCustomers;
module.exports.processRawCustomersList = processRawCustomersList;
