function degreeToRadians(degree) {
  return degree * Math.PI / 180;
}

const EARTH_RADIUS_KM = 6371;

//https://en.wikipedia.org/wiki/Great-circle_distance
module.exports = function calculateDistance(pointA, pointB) {
  const latitudeA = degreeToRadians(pointA.latitude);
  const latitudeB = degreeToRadians(pointB.latitude);
  const longitudeA = degreeToRadians(pointA.longitude);
  const longitudeB = degreeToRadians(pointB.longitude);
  const deltaLatitude = latitudeA - latitudeB;
  const deltaLongitude = longitudeA - longitudeB;
  const centralAngle = 2 * Math.asin(
    Math.sqrt(
      Math.pow( deltaLatitude/2 , 2) +
      Math.cos(latitudeA) * Math.cos(latitudeB) * Math.pow(deltaLongitude/2, 2)
    )
  );

  const distance = EARTH_RADIUS_KM * centralAngle;

  return distance;
};
