const flatten = require("./flatten");

const testCases = [
  {
    input: [1, 2, [3, 4, [5, 6, 7], 8], 9],
    expected: [1, 2, 3, 4, 5, 6, 7, 8, 9],
  },
  {
    input: [],
    expected: []
  },
  {
    input: [[[]]],
    expected: []
  }
]

//let's just mock some tests for the sake of excercise
function test(input, expected, testFunction) {
  const flattened = testFunction(input);

  if (flattened.length !== expected.length) {
    throw "The number of elements doesn't match";
  }

  for (let i = 0; i < expected.length; i++) {
    if (expected[i] !== flattened[i]) {
      throw "Element " + i + " doesn't match expected value";
    }
  }
}

try {

  testCases.forEach(({input, expected}) => {
    test(input, expected, flatten);
  })
  console.log("Success!");
} catch (e) {
  console.log("Error", e);
}
