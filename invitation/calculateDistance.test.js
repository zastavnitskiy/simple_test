const calculateDistance = require("./calculateDistance");
const PRECISION = 5; //for our use case with invitation 5 km is ok;

const cases = [
  {
    input: [
      { latitude: "53.0033946", longitude: "-6.3877505" },
      { latitude: "53.3393", longitude: "-6.2576841" }
    ],
    expected: 38.34390289045358
  },
  {
    input: [
      { latitude: "52.228056", longitude: "-7.915833" },
      { latitude: "53.3393", longitude: "-6.2576841" }
    ],
    expected: 166.44077672131803
  },
  {
    input: [
      { latitude: "53.3393", longitude: "-6.2576841" },
      { latitude: "53.3393", longitude: "-6.2576841" }
    ],
    expected: 0
  },
  {
    input: [{ latitude: 0, longitude: 0 }, { latitude: 0, longitude: 0 }],
    expected: 0
  }
];

try {
  cases.forEach(({ input, expected }) => {
    if (Math.abs(calculateDistance.apply(null, input) - expected) > PRECISION) {
      throw "Calculated distance doesn't match our expectation";
    }
  });
  console.log("Success!");
} catch (e) {
  console.log("Test Failed");
}
